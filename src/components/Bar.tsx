import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Avatar from "@material-ui/core/Avatar";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      position: "fixed",
      top: 0,
      width: "100%",
      zIndex: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  })
);

export default function AppBars() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const routePage = [
    { urlName: "หน้าแรก", url: "/screen/Home" },
    { urlName: "รายการสินค้า", url: "/screen/Payment" },
    { urlName: "วิธีการชำระเงิน", url: "/screen/Payment" },
    { urlName: "สมัครสมาชิก", url: "/screen/Payment" },
    { urlName: "เข้าสู่ระบบ", url: "/screen/Payment" },
  ];

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>

          <Typography variant="h6" className={classes.title}>
            APPONLINE
          </Typography>
          {routePage.map((item, index) => {
            return <Button color="inherit" href={item.url} key={index}>
              {item.urlName}
            </Button>;
          })}
          {/* <Button color="inherit" href="/screen/Payment">
            PAYMENT
          </Button>
          <Button color="inherit" href="/screen/Vedio">
            VEDIO
          </Button>
          <Button color="inherit" href="/screen/Game">
            GAME
          </Button>
          <Button color="inherit" href="/screen/Music">
            MUSIC
          </Button> */}
          <Button
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleClick}
          >
            <Avatar src="/images/tong.jpg"></Avatar>
          </Button>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={handleClose}>Profile</MenuItem>
            <MenuItem onClick={handleClose}>My account</MenuItem>
            <MenuItem onClick={handleClose}>Logout</MenuItem>
          </Menu>
        </Toolbar>
      </AppBar>
    </div>
  );
}
