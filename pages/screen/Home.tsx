import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Products from "./Products";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
      <div style={{width: '100%'}}>
        {children}
      </div>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    height: '100%',
    marginTop: 64
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    height: 'calc(100vh - 64px)',
    minWidth: 230,
    "& .MuiTab-wrapper": {
      flexDirection: "row",
      justifyContent: "flex-start"
    }
  },
  active: {
    background: theme.palette.primary.main,
    color: "#fff",
    fontSize: 18
  }
}));

export default function Home() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        <Tab className={value === 0 ? classes.active : ""} label="หมวดสินค้า1" {...a11yProps(0)} />
        <Tab className={value === 1 ? classes.active : ""} label="หมวดสินค้า2" {...a11yProps(1)} />
        <Tab className={value === 2 ? classes.active : ""} label="หมวดสินค้า3" {...a11yProps(2)} />
        <Tab className={value === 3 ? classes.active : ""} label="หมวดสินค้า4" {...a11yProps(3)} />
        <Tab className={value === 4 ? classes.active : ""} label="หมวดสินค้า5" {...a11yProps(4)} />
        <Tab className={value === 5 ? classes.active : ""} label="หมวดสินค้า6" {...a11yProps(5)} />
        <Tab className={value === 6 ? classes.active : ""} label="หมวดสินค้า7" {...a11yProps(6)} />
      </Tabs>
      <TabPanel value={value} index={0}>
        <Products />
      </TabPanel>
      <TabPanel value={value} index={1}>
        Item Two
      </TabPanel>
      <TabPanel value={value} index={2}>
        Item Three
      </TabPanel>
      <TabPanel value={value} index={3}>
        Item Four
      </TabPanel>
      <TabPanel value={value} index={4}>
        Item Five
      </TabPanel>
      <TabPanel value={value} index={5}>
        Item Six
      </TabPanel>
      <TabPanel value={value} index={6}>
        Item Seven
      </TabPanel>
    </div>
  );
}
