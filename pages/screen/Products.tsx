import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Grid, { GridSpacing } from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import VisibilityIcon from "@material-ui/icons/Visibility";
import ShareIcon from "@material-ui/icons/Share";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import AddIcon from "@material-ui/icons/Add";
import SendIcon from "@material-ui/icons/Send";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { Paper } from "@material-ui/core";
import Link from "@material-ui/core/Link";
import theme from "../../src/theme/theme";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: "80%", // 16:9
    },
    control: {
      padding: theme.spacing(2),
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    avatar: {
      backgroundColor: red[500],
    },
    Grids: {
      margin: 0,
      width: "100%",
      overflowY: "scroll",
      height: "calc(100vh - 64px)",
      background: "#ececec",
    },
    bath: {
      background: theme.palette.primary.main,
      width: "100%",
      padding: 5,
      position: "fixed",
      right: 1,
    },
    cardHeader: {
      padding: 0,
      justifyContent: "flex-end",
      "& .MuiCardHeader-content": {
        flex: "none",
      },
      // "& .MuiCardHeader-root": {
      //     flex: 'none',
      //     display: 'flex',
      //     padding: 0,
      //     alignItems: 'center',
      // }
    },
  })
);

const itemlist = [
  { p_name: "product1", p_price: "10000.0", p_detail: "detail1" },
  { p_name: "product2", p_price: "200.0", p_detail: "detail2" },
  { p_name: "product3", p_price: "200.0", p_detail: "detail3" },
  { p_name: "product4", p_price: "200.0", p_detail: "detail4" },
  { p_name: "product5", p_price: "100.0", p_detail: "detail5" },
  { p_name: "product6", p_price: "200.0", p_detail: "detail6" },
  { p_name: "product7", p_price: "200.0", p_detail: "detail7" },
  { p_name: "product8", p_price: "200.0", p_detail: "detail8" },
  { p_name: "product9", p_price: "200.0", p_detail: "detail9" },
  { p_name: "product10", p_price: "200.0", p_detail: "detail10" },
  { p_name: "product11", p_price: "200.0", p_detail: "detail11" },
  { p_name: "product12", p_price: "200.0", p_detail: "detail12" },
  { p_name: "product13", p_price: "200.0", p_detail: "detail13" },
  { p_name: "product14", p_price: "200.0", p_detail: "detail14" },
  { p_name: "product15", p_price: "200.0", p_detail: "detail15" },
  { p_name: "product16", p_price: "200.0", p_detail: "detail16" },
];

export default function Products() {
  const classes = useStyles();
  return (
    // <div style={{ width: "100%", margin: 10 }}>
    <Grid container spacing={2} className={classes.Grids}>
      <Grid item xs={12}>
        <div>หมวดสินค้า1</div>
      </Grid>
      {itemlist.map((item: any, index) => {
        return (
          <Grid item xl={2} lg={2} md={4} xs={6} key={index}>
            <Card>
              {/* <div className={classes.bath}>
                <div style={{padding: 10}}>{item.p_price}</div>
              </div> */}
              <CardHeader
                className={classes.cardHeader}
                // avatar={
                //   <Avatar
                //     src="/images/tong.jpg"
                //     className={classes.avatar}
                //   ></Avatar>
                // }
                // action={
                //   <IconButton aria-label="settings">
                //     <MoreVertIcon />
                //   </IconButton>
                // }
                title={
                  <div
                    style={{
                      background: theme.palette.primary.main,
                      padding: 5,
                      color: "#fff",
                    }}
                  >
                    {item.p_price}
                  </div>
                }
                // subheader="September 14, 2016"
              />
              <CardMedia
                className={classes.media}
                image="/images/products/product1.png"
                title="Paella dish"
              />
              <CardContent style={{ padding: 0 }}>
                {/* <Typography gutterBottom variant="h5" component="h2" style={{padding: 0}}>
                  {item.p_name}
                </Typography> */}
                <IconButton>
                  <VisibilityIcon style={{ fontSize: 14 }} />
                  <span style={{ fontSize: 12, paddingLeft: 5 }}>
                    จำนวนครั้ง 120
                  </span>
                </IconButton>
              </CardContent>
              <CardActions disableSpacing style={{ padding: 5 }}>
                <Grid item xs={6}>
                  <IconButton aria-label="share">
                    <ThumbUpIcon />
                  </IconButton>
                  <IconButton aria-label="add to ShareIcon">
                    <ShareIcon />
                  </IconButton>
                </Grid>
                <Grid item xs={6} style={{ textAlign: "right" }}>
                  <IconButton>
                    <AddIcon />
                  </IconButton>
                  <IconButton>
                    <SendIcon />
                  </IconButton>
                </Grid>
              </CardActions>
            </Card>
          </Grid>
        );
      })}
    </Grid>
    // </div>
  );
}
